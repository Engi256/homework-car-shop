//
//  CollectionViewCell.m
//  Homework5.2
//
//  Created by User on 05.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "CarCollectionViewCell.h"

@interface CarCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation CarCollectionViewCell


- (void) setImage:(UIImage *)image {
    [self.imageView setImage:image];
}

@end

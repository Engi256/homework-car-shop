//
//  DateUtils.m
//  Homework5.2
//
//  Created by User on 16.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "DateUtils.h"

static NSDateFormatter* formatter;

@implementation DateUtils

+ (void)initialize {
    formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd.MM.yyyy"];
}

+ (NSDate*) dateFromString: (NSString*) dateString {
    return [formatter dateFromString:dateString];
}

+ (NSString *)stringFromDate:(NSDate *)date {
    return [formatter stringFromDate:date];
}

@end

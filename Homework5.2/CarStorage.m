//
// Created by User on 10.05.16.
// Copyright (c) 2016 User. All rights reserved.
//

#import "CarStorage.h"
#import "Car.h"
#import "CarFilter.h"
#import "DateUtils.h"


static NSArray<Car*>* cars;
static void *ObserverObservingContext = &ObserverObservingContext;

@implementation CarStorage

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    
    if(self) {
        [self initCars];
        _filter = [CarFilter new];
    }
    
    return self;
}

- (void)initCars {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    Car* car1 = [[Car alloc] initWithID:1 name:@"Lamborghini Aventator" shortInfo:@"Dream car" isFavorite:[userDefaults boolForKey:@"1"]
                                  price:@10000000 date:[DateUtils dateFromString:@"12.12.2016"] bodyType:BodyTypeUniversal
                              imagePool:[CarStorage imagesByCarName:@"lambo"]];
    [car1 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];


    Car* car2 = [[Car alloc] initWithID:2 name:@"Volvo V40 Cross Country" shortInfo:@"Safe car"
                             isFavorite:[userDefaults boolForKey:@"2"] price:@2000000 date:[DateUtils dateFromString:@"12.12.2015"]
                             bodyType:BodyTypeHatchback imagePool:[CarStorage imagesByCarName:@"volvo"]];
    [car2 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];


    Car* car3 = [[Car alloc] initWithID:3 name:@"Kia Sportage" shortInfo:@"Normal car"
                             isFavorite:[userDefaults boolForKey:@"3"] price:@700000 date:[DateUtils dateFromString:@"12.12.2014"]
                             bodyType:BodyTypeCrossover imagePool:[CarStorage imagesByCarName:@"kia"]];
    [car3 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];


    Car* car4 = [[Car alloc] initWithID:4 name:@"BWM M5" shortInfo:@"Expensive car"
                             isFavorite:[userDefaults boolForKey:@"4"] price:@3000000 date:[DateUtils dateFromString:@"12.12.2013"]
                             bodyType:BodyTypeSedan imagePool:[CarStorage imagesByCarName:@"bmw"]];
    [car4 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];


    Car* car5 = [[Car alloc] initWithID:5 name:@"Volkswagen Passat CC" shortInfo:@"Business car"
                             isFavorite:[userDefaults boolForKey:@"5"] price:@2000000 date:[DateUtils dateFromString:@"12.12.2012"]
                             bodyType:BodyTypeSedan imagePool:[CarStorage imagesByCarName:@"vw"]];
    [car5 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];


    Car* car6 = [[Car alloc] initWithID:6 name:@"Hyundai Solaris" shortInfo:@"Another normal car"
                             isFavorite:[userDefaults boolForKey:@"6"] price:@700000 date:[DateUtils dateFromString:@"12.12.2011"]
                             bodyType:BodyTypeSedan imagePool:[CarStorage imagesByCarName:@"hyundai"]];
    [car6 addObserver:self forKeyPath:@"isFavorite" options:NSKeyValueObservingOptionNew context:ObserverObservingContext];

    cars = @[car1, car2, car3, car4, car5, car6];
}

+ (NSArray*)imagesByCarName: (NSString*) carName {
    NSMutableArray* images = [NSMutableArray new];
    NSInteger carImgIndex = 1;
    while (YES) {
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@%ld.jpg", carName, (long)carImgIndex]];
        if(img) {
            [images addObject:img];
            carImgIndex++;
        } else {
            return images.copy;
        }
    }
}

#pragma mark - Getting data

- (NSArray*) allCars {
    return cars;
}

- (NSArray*) favoriteCars {
    NSMutableArray* favoriteCars = [NSMutableArray new];
    for (Car* car in cars) {
        if(car.isFavorite) {
            [favoriteCars addObject:car];
        }
    }

    return favoriteCars.copy;
}

- (NSArray *) filteredCars {
    NSMutableArray* filteredCars = [NSMutableArray new];
    if(self.filter) {
        BOOL isSedan = [self.filter.bodyTypes[@"Sedan"] boolValue];
        BOOL isHatchback = [self.filter.bodyTypes[@"Hatchback"] boolValue];
        BOOL isUniversal = [self.filter.bodyTypes[@"Universal"] boolValue];
        BOOL isCrossover = [self.filter.bodyTypes[@"Crossover"] boolValue];
        NSNumber* price = self.filter.price;
        NSDate* date = self.filter.productionDate;
        for (Car* car in cars) {
            if((isSedan && (car.bodyType == BodyTypeSedan))
                    || (isHatchback && (car.bodyType == BodyTypeHatchback))
                    || (isUniversal && (car.bodyType == BodyTypeUniversal))
                    || (isCrossover && (car.bodyType == BodyTypeCrossover))) {
                if(!price || [car.price floatValue] <= [price floatValue]) {
                    if(!date || [car.date compare:date] == NSOrderedAscending ) {
                        [filteredCars addObject:car];
                    }
                }
            }
        }
        return filteredCars.copy;
    } else {
        return [self allCars];
    }
}

#pragma mark - Observering

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (context == ObserverObservingContext) {
        BOOL newValue = [change[NSKeyValueChangeNewKey] boolValue];
        Car* car = (Car*) object;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:newValue forKey:[NSString stringWithFormat:@"%li", (long)car.ID]];
        [defaults synchronize];
    }
}

- (void) dealloc {
    for (Car* car in cars) {
        [car removeObserver:self forKeyPath:@"isFavorite"];
    }
}

@end
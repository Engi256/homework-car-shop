//
//  CollectionViewCell.h
//  Homework5.2
//
//  Created by User on 05.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarCollectionViewCell : UICollectionViewCell

- (void) setImage: (UIImage*) image;

@end

//
//  CarSource.h
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

typedef NS_ENUM(NSInteger, BodyType) {
    BodyTypeSedan = 0,
    BodyTypeHatchback = 1,
    BodyTypeUniversal = 2,
    BodyTypeCrossover = 3
};

@interface Car : NSObject

@property(readonly) NSInteger ID;
@property(readonly) NSString *name;
@property(readonly) NSString *shortInfo;
@property(nonatomic) BOOL isFavorite;
@property(readonly) NSNumber *price;
@property(readonly) NSDate *date;
@property(readonly) BodyType bodyType;
@property(readonly) NSArray *imagePool;

- (instancetype)initWithID:(NSInteger)ID name:(NSString *)name shortInfo:(NSString *)shortInfo isFavorite:(BOOL)isFavorite
                     price:(NSNumber *)price date:(NSDate *)date bodyType:(BodyType)bodyType imagePool:(NSArray *)imagePool;

+ (instancetype)carWithID:(NSInteger)ID name:(NSString *)name shortInfo:(NSString *)shortInfo isFavorite:(BOOL)isFavorite
                    price:(NSNumber *)price date:(NSDate *)date bodyType:(BodyType)bodyType imagePool:(NSArray *)imagePool;


@end

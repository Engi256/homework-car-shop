//
// Created by User on 12.05.16.
// Copyright (c) 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CarFilter : NSObject

@property NSMutableDictionary* bodyTypes;
@property NSDate* productionDate;
@property NSNumber* price;

@end
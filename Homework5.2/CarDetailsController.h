//
//  CarDetailsController.h
//  Homework5.2
//
//  Created by User on 05.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"

@interface CarDetailsController : UIViewController

@property Car* car;

@end

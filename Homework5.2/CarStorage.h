//
// Created by User on 10.05.16.
// Copyright (c) 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CarFilter;


@interface CarStorage : NSObject

- (NSArray*) allCars;
- (NSArray*) favoriteCars;
- (NSArray*) filteredCars;

@property CarFilter* filter;

@end
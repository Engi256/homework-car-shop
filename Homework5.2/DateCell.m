//
//  DateCell.m
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "DateCell.h"

@interface DateCell ()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation DateCell

- (IBAction) dateChanged {
    [self.delegate dateValueChanged: self.datePicker.date];
}

- (void)setDate:(NSDate *)date {
    self.datePicker.date = date;
}

@end

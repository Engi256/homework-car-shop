//
//  DateUtils.h
//  Homework5.2
//
//  Created by User on 16.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSDate*) dateFromString: (NSString*) dateString;
+ (NSString*) stringFromDate: (NSDate*) date;

@end

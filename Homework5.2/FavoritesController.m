//
//  FavoritesController.m
//  Homework5.2
//
//  Created by User on 08.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "FavoritesController.h"
#import "Car.h"
#import "CarCell.h"
#import "CarDetailsController.h"
#import "CarStorage.h"

@interface FavoritesController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray* favoriteCars;
@property CarStorage *storage;

@end


static NSString* reuseIdentifier = @"reuseId";

@implementation FavoritesController

- (instancetype)initWithStorage:(CarStorage *)storage {
    self = [super init];

    if(self) {
        _storage = storage;
        self.navigationItem.title = @"Favorites";
    }

    return self;
}

#pragma mark - Controller lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CarCell class]) bundle:nil]
         forCellReuseIdentifier:reuseIdentifier];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.favoriteCars = [self.storage favoriteCars];
    [self.tableView reloadData];
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.favoriteCars.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CarCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    Car* car = self.favoriteCars[indexPath.row];
    [cell setName:car.name];
    [cell setShortInfo:car.shortInfo];
    [cell setImage:car.imagePool[0]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CarDetailsController* dvc = [CarDetailsController new];
    dvc.car = self.favoriteCars[indexPath.row];
    [self.navigationController pushViewController:dvc animated: YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Unfavorite";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Car* car = self.favoriteCars[indexPath.row];
        car.isFavorite = NO;
        self.favoriteCars = [self.storage favoriteCars];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } 
}

@end

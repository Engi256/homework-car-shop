//
//  AppDelegate.m
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "AppDelegate.h"
#import "AllItemsController.h"
#import "FavoritesController.h"
#import "CarStorage.h"

@interface AppDelegate ()
    
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [UIWindow new];

    CarStorage* storage = [CarStorage new];

    AllItemsController* vc1 = [[AllItemsController alloc] initWithStorage: storage];
    FavoritesController* vc2 = [[FavoritesController alloc] initWithStorage: storage];
    UINavigationController* nc1 = [[UINavigationController alloc] initWithRootViewController:vc1];
    UINavigationController* nc2 = [[UINavigationController alloc] initWithRootViewController:vc2];
    nc1.title = @"All items";
    nc2.title = @"Favorites";
    UITabBarController *tabBarCtrl = [UITabBarController new];
    tabBarCtrl.viewControllers = @[nc1, nc2];
    
    self.window.rootViewController = tabBarCtrl;
    
    [self.window makeKeyAndVisible];
    
    self.window.frame = [UIScreen mainScreen].bounds;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//
//  FilterController.h
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"

@class CarStorage;

@interface FilterController : UIViewController

- (instancetype)initWithStorage: (CarStorage*) storage;

@end

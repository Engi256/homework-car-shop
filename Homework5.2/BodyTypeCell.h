//
//  BodyTypeCell.h
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BodyTypeCellDelegate;

@interface BodyTypeCell : UITableViewCell

@property id<BodyTypeCellDelegate> delegate;

- (void) setIsOn:(BOOL)value;
- (void) setText:(NSString *)text;

@end

@protocol BodyTypeCellDelegate <NSObject>

- (void) bodyTypeCell:(NSString *)bodyTypeName valueChanged:(BOOL)newValue;

@end

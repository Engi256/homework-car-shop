//
// Created by User on 12.05.16.
// Copyright (c) 2016 User. All rights reserved.
//

#import "CarFilter.h"


@implementation CarFilter

- (instancetype)init {
    self = [super init];
    if(self) {
        _bodyTypes = @{@"Sedan" : [NSNumber numberWithBool:YES],
                       @"Hatchback" : [NSNumber numberWithBool:YES],
                       @"Universal" : [NSNumber numberWithBool:YES],
                       @"Crossover" : [NSNumber numberWithBool:YES]}.mutableCopy;
    }

    return self;
}

@end
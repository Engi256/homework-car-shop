//
//  CarSource.m
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "Car.h"

@implementation Car

- (instancetype)initWithID:(NSInteger)ID name:(NSString *)name shortInfo:(NSString *)shortInfo isFavorite:(BOOL)isFavorite
                     price:(NSNumber *)price date:(NSDate *)date bodyType:(BodyType)bodyType imagePool:(NSArray *)imagePool {
    self = [super init];
    if (self) {
        _ID = ID;
        _name = name;
        _shortInfo = shortInfo;
        self.isFavorite = isFavorite;
        _price = price;
        _date = date;
        _bodyType = bodyType;
        _imagePool = imagePool;
    }

    return self;
}

+ (instancetype)carWithID:(NSInteger)ID name:(NSString *)name shortInfo:(NSString *)shortInfo isFavorite:(BOOL)isFavorite
                    price:(NSNumber *)price date:(NSDate *)date bodyType:(BodyType)bodyType imagePool:(NSArray *)imagePool {
    return [[self alloc] initWithID:ID name:name shortInfo:shortInfo isFavorite:isFavorite price:price date:date bodyType:bodyType imagePool:imagePool];
}


@end

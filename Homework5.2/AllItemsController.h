//
//  AllItemsController.h
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarStorage;

@interface AllItemsController : UIViewController

- (instancetype) initWithStorage: (CarStorage*) storage;

@end

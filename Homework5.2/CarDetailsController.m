//
//  CarDetailsController.m
//  Homework5.2
//
//  Created by User on 05.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "CarDetailsController.h"
#import "CarCollectionViewCell.h"
#import "DateUtils.h"

@interface CarDetailsController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productionDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortInfoLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *favoriteSegmentedControl;

@end

static NSString* reuseId = @"reuseId";

@implementation CarDetailsController

#pragma mark - View controller lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setImageInView: 0];
    
    self.priceLabel.text = [NSString stringWithFormat:@"%@", self.car.price];
    self.productionDateLabel.text = [DateUtils stringFromDate:self.car.date];
    self.shortInfoLabel.text = self.car.shortInfo;
    [self updateFavoriteSegmentedControlValue];
    switch (self.car.bodyType) {
        case BodyTypeHatchback:
            self.bodyTypeLabel.text = @"Hatchback";
            break;
        case BodyTypeSedan:
            self.bodyTypeLabel.text = @"Sedan";
            break;
        case BodyTypeUniversal:
            self.bodyTypeLabel.text = @"Universal";
            break;
        case BodyTypeCrossover:
            self.bodyTypeLabel.text = @"Crossover";
            break;
        default:
            break;
    }
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CarCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:reuseId];
}

- (void)updateFavoriteSegmentedControlValue {
    [self.favoriteSegmentedControl setSelectedSegmentIndex:self.car.isFavorite ? 0 : 1];
}

- (void) viewWillAppear:(BOOL)animated {
    [self updateFavoriteSegmentedControlValue];
}

- (void)setImageInView: (NSInteger) imageIndex {
    self.imageView.image = self.car.imagePool[imageIndex];
}

- (IBAction) setFavorite {
    self.car.isFavorite = self.favoriteSegmentedControl.selectedSegmentIndex == 0;
}

#pragma mark - Collection view data source and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.car.imagePool.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId forIndexPath:indexPath];
    [cell setImage:self.car.imagePool[indexPath.row]];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self setImageInView: indexPath.row];
}

@end

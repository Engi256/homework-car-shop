//
//  FavoritesController.h
//  Homework5.2
//
//  Created by User on 08.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarStorage;

@interface FavoritesController : UIViewController

- (instancetype) initWithStorage: (CarStorage*) storage;

@end

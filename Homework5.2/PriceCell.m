//
//  PriceCell.m
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "PriceCell.h"

@interface PriceCell ()

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *currentPriceLabel;

@end

@implementation PriceCell

- (IBAction) sliderChanged {
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%.0f", self.slider.value];
    [self.delegate priceCellValueChanged:@(self.slider.value)];
}

- (void)setValue:(NSNumber *)value {
    self.slider.value = [value floatValue];
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%.0f", self.slider.value];
}


@end

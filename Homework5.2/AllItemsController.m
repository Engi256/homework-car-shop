//
//  AllItemsController.m
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "AllItemsController.h"
#import "CarCell.h"
#import "Car.h"
#import "FilterController.h"
#import "CarDetailsController.h"
#import "CarStorage.h"

@interface AllItemsController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray<Car*>* cars;
@property CarStorage* storage;
@property BOOL isFiltered;

@end


static NSString* reuseIdentifier = @"reuse id";

@implementation AllItemsController


- (instancetype)initWithStorage:(CarStorage *)storage {
    self = [super init];

    if(self) {
        _storage = storage;
        self.navigationItem.title = @"All items";
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Filter"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(onFilterBtn)];
    }

    return self;
}

#pragma mark - View controller lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CarCell class]) bundle:nil]
         forCellReuseIdentifier:reuseIdentifier];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}

- (void) onFilterBtn {
    FilterController *filterVC = [[FilterController alloc] initWithStorage:self.storage];
    self.isFiltered = YES;
    [self.navigationController pushViewController:filterVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.isFiltered) {
        self.cars = [self.storage filteredCars];
        [self.tableView reloadData];
    } else {
        self.cars = [self.storage allCars];
        [self.tableView reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cars.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CarCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    Car* car = self.cars[indexPath.row];
    [cell setName:car.name];
    [cell setShortInfo:car.shortInfo];
    [cell setImage:car.imagePool[0]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CarDetailsController* dvc = [CarDetailsController new];
    dvc.car = self.cars[indexPath.row];
    [self.navigationController pushViewController:dvc animated: YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Car* car = self.cars[indexPath.row];
    
    NSString* title;
    if(car.isFavorite) {
        title = @"Unfavorite";
    } else {
        title = @"Favorite";
    }
   
    NSMutableArray<UITableViewRowAction *> *actions = [NSMutableArray new];
    UITableViewRowAction *favAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                         title:title
                                                                       handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                                                                           car.isFavorite = !car.isFavorite;
                                                                           [tableView setEditing:NO];
                                                                       }];
    
    if(!car.isFavorite) {
        favAction.backgroundColor = [UIColor greenColor];
    } else {
        favAction.backgroundColor = [UIColor redColor];
    }
    
    [actions addObject:favAction];
    
    return actions.copy;
}

@end

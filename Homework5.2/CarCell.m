//
//  CarCell.m
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "CarCell.h"

@interface CarCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortInfoLabel;

@end

@implementation CarCell

- (void)setName:(NSString *) name {
    self.nameLabel.text = name;
}

- (void) setShortInfo:(NSString *)shortInfo {
    self.shortInfoLabel.text = shortInfo;
}

- (void) setImage:(UIImage *)image {
    [self.imgView setImage:image];
}

@end

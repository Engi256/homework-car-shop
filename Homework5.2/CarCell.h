//
//  CarCell.h
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarCell : UITableViewCell

- (void) setName: (NSString*) name;
- (void) setShortInfo: (NSString*) shortInfo;
- (void) setImage: (UIImage*) image;

@end

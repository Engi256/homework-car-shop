//
//  BodyTypeCell.m
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "BodyTypeCell.h"

@interface BodyTypeCell ()

@property (weak, nonatomic) IBOutlet UILabel *bodyTypeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *bodyTypeSwitch;

@end

@implementation BodyTypeCell

- (IBAction) switchChanged {
    [self.delegate bodyTypeCell:self.bodyTypeLabel.text valueChanged:self.bodyTypeSwitch.isOn];
}

- (void) setText:(NSString *)text {
    self.bodyTypeLabel.text = text;
}

- (void) setIsOn:(BOOL)value {
    [self.bodyTypeSwitch setOn:value];
}

@end

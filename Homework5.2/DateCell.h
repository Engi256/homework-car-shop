//
//  DateCell.h
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DateCellDelegate;

@interface DateCell : UITableViewCell

@property id<DateCellDelegate> delegate;

- (void)setDate:(NSDate *)date;

@end

@protocol DateCellDelegate <NSObject>

- (void) dateValueChanged : (NSDate*)newDate;

@end

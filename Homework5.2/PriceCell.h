//
//  PriceCell.h
//  Homework5.2
//
//  Created by User on 06.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PriceCellDelegate;

@interface PriceCell : UITableViewCell

@property id<PriceCellDelegate> delegate;

- (void)setValue:(NSNumber *)value;

@end


@protocol PriceCellDelegate <NSObject>

- (void)priceCellValueChanged: (NSNumber*)newPrice;

@end
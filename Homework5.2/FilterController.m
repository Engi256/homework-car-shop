//
//  FilterController.m
//  Homework5.2
//
//  Created by User on 04.05.16.
//  Copyright © 2016 User. All rights reserved.
//

#import "FilterController.h"
#import "BodyTypeCell.h"
#import "DateCell.h"
#import "PriceCell.h"
#import "CarStorage.h"
#import "CarFilter.h"

@interface FilterController () <UITableViewDataSource, PriceCellDelegate, BodyTypeCellDelegate, DateCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (readonly) CarStorage* storage;
@property CarFilter* filter;

@end

static NSString* enumReuseId = @"enumReuseId";
static NSString* dateReuseId = @"dateReuseId";
static NSString* sliderReuseId = @"sliderReuseId";
NSArray* bodyTypeNames;

typedef NS_ENUM(NSInteger, Sections){
    BodyTypeSection = 0, PriceSection = 1, DateSection = 2
};

@implementation FilterController

- (instancetype)initWithStorage: (CarStorage *) storage {
    self = [super init];
    
    if(self) {
        _storage = storage;
        _filter = storage.filter;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BodyTypeCell class]) bundle:nil] forCellReuseIdentifier: enumReuseId];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DateCell class]) bundle:nil] forCellReuseIdentifier: dateReuseId];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PriceCell class]) bundle:nil] forCellReuseIdentifier: sliderReuseId];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    bodyTypeNames = @[@"Sedan", @"Hatchback", @"Universal", @"Crossover"];
}


#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case BodyTypeSection:
            return @"Body Type";
        case PriceSection :
            return @"Price";
        case DateSection :
            return @"Date";
        default:
            return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case BodyTypeSection:
            return 4;
        case PriceSection :
        case DateSection :
            return 1;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == PriceSection) {
        PriceCell* cell = [tableView dequeueReusableCellWithIdentifier:sliderReuseId forIndexPath:indexPath];
        cell.delegate = self;
        if(self.filter.price) {
            cell.value = self.filter.price;
        }
        return cell;
    } else if(indexPath.section == DateSection) {
        DateCell* cell = [tableView dequeueReusableCellWithIdentifier:dateReuseId forIndexPath:indexPath];
        cell.delegate = self;
        if(self.filter.productionDate) {
            cell.date = self.filter.productionDate;
        }
        return cell;
    } else if(indexPath.section == BodyTypeSection) {
        BodyTypeCell* cell = [tableView dequeueReusableCellWithIdentifier:enumReuseId forIndexPath:indexPath];
        cell.delegate = self;
        [cell setText: bodyTypeNames[indexPath.row]];
        [cell setIsOn: self.filter.bodyTypes[bodyTypeNames[indexPath.row]]];
        return cell;
    }
    
    return nil;
}


#pragma mark - Delegates from details

- (void) dateValueChanged : (NSDate*) newValue {
    self.filter.productionDate = newValue;
}

- (void) bodyTypeCell:(NSString *)bodyTypeName valueChanged:(BOOL)newValue {
    self.filter.bodyTypes[bodyTypeName] = [NSNumber numberWithBool:newValue];
}

- (void)priceCellValueChanged:(NSNumber *)newValue {
    self.filter.price = newValue;
}

@end
